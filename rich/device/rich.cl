// Copyright (C) 2013-2016 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

 // ACL kernel for adding two input vectors

__attribute__((packed))
__attribute__((aligned(16)))
struct rich_photon_in
{
float emissionPoint_X;
float emissionPoint_Y;
float emissionPoint_Z;
float CoC_X;
float CoC_Y;
float CoC_Z;
float virtDetPoint_X;
float virtDetPoint_Y;
float virtDetPoint_Z;
float radius;
};


__attribute__((packed))
__attribute__((aligned(16)))
struct rich_photon_out
{
float sphReflPoint_X;
float sphReflPoint_Y;
float sphReflPoint_Z;
}; 

__attribute__((num_simd_work_items(4)))
__attribute__((reqd_work_group_size(256,1,1)))
__attribute__((num_compute_units(1)))
__kernel void rich(__global const struct rich_photon_in* restrict dataIn,
				   __global struct rich_photon_out* restrict dataOut)
{
    // get index of the work item
    private int index = get_global_id(0);

	private float evec_x=0,evec_y=0,evec_z=0;
	private float e2=0;
	
	private float dvec_x=0,dvec_y=0,dvec_z=0;
	private float d2=0;
	
	private float e=0;
	private float d=0;
	private float cosgamma=0;
	private float singamma=0;
	
	private float dx=0;
	private float edx=0;
	private float dy=0;
	private float dy2=0;
	private float r2=0;
	
	private float a0=0;
	private float inv_a0=0;
	private float a1=0;
	private float a2=0;
	private float a3=0;
	private float a4=0;
	
	private const float r4 = 1.0 / 4.0;
	private const float q2 = 1.0 / 2.0;
	private const float q8 = 1.0 / 8.0;
	private const float q1 = 3.0 / 8.0;
	private const float q3 = 3.0 / 16.0;
	private const float UU = -(sqrt(3.0) / 2.0 );
	
	private float aa=0;
	private float pp=0;
	private float qq=0;
	private float rr=0;
	private float rc=0;
	private float sc=0;
	private float tc=0;
	
	private float qcub=0;
	private float rcub=0;
	
	private float Q=0;
	private float R=0;
	
	private float Q3=0;
	private float R2=0;
	private float sgnR=0;
	
	private float tocqrt=0;
	
	private float A=0;
	private float B=0;
	private float u1=0;
	private float u2=0;
	
	private float w1_x=0;
	private float w1_y=0;
	private float w2_x=0;
	private float w2_y=0;
	private float w3_x=0;
	private float w3_y=0;
	
	private float res=0;
	
	private float sinbeta=0;
	
	private float cosbeta=0;
	private float OneMcosbeta=0;
	
	private float n_x=0,n_y=0,n_z=0;
	private float norm_n;
	private float n_x_norm=0,n_y_norm=0,n_z_norm=0;
	
	private float RotM_11=0;
	private float RotM_12=0;
	private float RotM_13=0;
	private float RotM_21=0;
	private float RotM_22=0;
	private float RotM_23=0;
	private float RotM_31=0;
	private float RotM_32=0;
	private float RotM_33=0;
	


	evec_x=(dataIn[index].emissionPoint_X - dataIn[index].CoC_X);
	evec_y=(dataIn[index].emissionPoint_Y - dataIn[index].CoC_Y);
	evec_z=(dataIn[index].emissionPoint_Z - dataIn[index].CoC_Z);
	
	e2 = evec_x*evec_x +evec_y*evec_y +evec_z*evec_z;
	
	dvec_x=(dataIn[index].virtDetPoint_X - dataIn[index].CoC_X);
	dvec_y=(dataIn[index].virtDetPoint_Y - dataIn[index].CoC_Y);
	dvec_z=(dataIn[index].virtDetPoint_Z - dataIn[index].CoC_Z);
	
	d2 = dvec_x*dvec_x +dvec_y*dvec_y +dvec_z*dvec_z;	
	
	e = sqrt(e2);
	d = sqrt(d2);
	cosgamma = (evec_x*dvec_x+evec_y*dvec_y+evec_z*dvec_z)/(e*d);
	singamma = sqrt(1.0 - cosgamma*cosgamma);
	
	dx = d * cosgamma;
	edx = e + dx;
	dy = d * singamma;
	dy2 = dy * dy;
	r2 = dataIn[index].radius * dataIn[index].radius;
	
	a0 =  4.0 * e2 * d2;
	inv_a0 = (1.0 / a0);
	a1 = -(4.0 * e2 * dy * dataIn[index].radius)*inv_a0;
	a2 = ( (dy2 * r2) + (edx * edx * r2) - a0)*inv_a0;
	a3 = (2.0 * e * dy * (e-dx)*dataIn[index].radius)*inv_a0;
	a4 = ( (e2 - r2) *dy2)*inv_a0;

	aa = a1 * a1;
	pp = a2 - q1 * aa;
	qq = a3 - q2 * a1 * (a2 - r4 * aa);
	rr = a4 -r4 * (a1 * a3 - r4 * aa * (a2 - q3 * aa));
	rc = q2 * pp;
	sc = r4 * (r4 * pp * pp - rr);
	tc = -(q8 * qq * q8 * qq);
	
	qcub = (rc * rc - 3.0 * sc);
	rcub = (2.0 * rc * rc * rc - 9.0 * rc * sc + 27.0 * tc);
	
	Q = qcub /  9.0;
	R = rcub / 54.0;
	
	Q3 = Q * Q * Q;
	R2 = R * R;
	
	sgnR = ( R >= 0 ? -1 : 1 );
	
	tocqrt = fabs(R) + sqrt( fabs(R2 - Q3) );
	
	
	A = sgnR * cbrt( tocqrt );
	
	B = Q / A;
	
	u1 = -0.5 * (A + B) - rc/3.0;
	u2 = UU * fabs(A - B);
	
	w1_x = sqrt( (sqrt(u1 * u1 + u2 * u2) + u1)/2.0 ); 
	w1_y = sqrt( (sqrt(u1 * u1 + u2 * u2) - u1)/2.0 );

	w2_x = sqrt( (sqrt(u1 * u1 + (-u2) * (-u2)) + u1)/2.0 ); 	
	w2_y = sqrt( (sqrt(u1 * u1 + (-u2) * (-u2)) - u1)/2.0 ); 
	
	if(fabs(w1_x * w2_x - w1_y * w2_y ) != 0.0)
	{
		if(fabs(w1_x * w2_y + w1_y * w2_x ) != 0.0)
		{
			w3_x = (-qq / 8.0)/ ( w1_x * w1_x + w1_y * w1_y);
		}	 
	}
	
	res = w1_x + w2_x + w3_x - r4*a1;
	
	sinbeta = res;
	
	if(res > 1.0)
	{
		sinbeta = 1.0;
	}
	
	if(res < -1.0)
	{
		sinbeta = -1.0;	
	}
	
	cosbeta = sqrt(1.0 - sinbeta*sinbeta);
	
	OneMcosbeta = 1 - cosbeta;
	
	n_x = (evec_y*dvec_z - evec_z*dvec_y);
	n_y = (evec_z*dvec_x - evec_x*dvec_z);
	n_z = (evec_x*dvec_y - evec_y*dvec_x);
	
	norm_n = sqrt(n_x*n_x + n_y*n_y + n_z*n_z);
	
	n_x_norm = n_x / norm_n; 
	n_y_norm = n_y / norm_n; 
	n_z_norm = n_z / norm_n; 
	
	
	
	RotM_11 = n_x_norm * n_x_norm * OneMcosbeta + cosbeta;
	RotM_12 = n_x_norm * n_y_norm * OneMcosbeta - n_z_norm * sinbeta;
	RotM_13 = n_x_norm * n_z_norm * OneMcosbeta + n_y_norm * sinbeta;
	
	RotM_21 = n_y_norm * n_x_norm * OneMcosbeta + n_z_norm * sinbeta;
	RotM_22 = n_y_norm * n_y_norm * OneMcosbeta + cosbeta;
	RotM_23 = n_y_norm * n_z_norm * OneMcosbeta - n_x_norm * sinbeta;
	
	RotM_31 = n_z_norm * n_x_norm * OneMcosbeta - n_y_norm * sinbeta;
	RotM_32 = n_z_norm * n_y_norm * OneMcosbeta + n_x_norm * sinbeta;
	RotM_33 = n_z_norm * n_z_norm * OneMcosbeta + cosbeta;
	
	
	dataOut[index].sphReflPoint_X = dataIn[index].CoC_X + (RotM_11 * evec_x + RotM_12 * evec_y + RotM_13 * evec_z)*(dataIn[index].radius/e);
	dataOut[index].sphReflPoint_Y = dataIn[index].CoC_Y + (RotM_21 * evec_x + RotM_22 * evec_y + RotM_23 * evec_z)*(dataIn[index].radius/e);
	dataOut[index].sphReflPoint_Z = dataIn[index].CoC_Z + (RotM_31 * evec_x + RotM_32 * evec_y + RotM_33 * evec_z)*(dataIn[index].radius/e);
	
}

