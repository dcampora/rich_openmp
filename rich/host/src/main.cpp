// Copyright (C) 2013-2016 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

///////////////////////////////////////////////////////////////////////////////////
// This host program executes a vector addition kernel to perform:
//  C = A + B
// where A, B and C are vectors with N elements.
//
// This host program supports partitioning the problem across multiple OpenCL
// devices if available. If there are M available devices, the problem is
// divided so that each device operates on N/M points. The host program
// assumes that all devices are of the same type (that is, the same binary can
// be used), but the code can be generalized to support different device types
// easily.
//
// Verification is performed against the same computation on the host CPU.
///////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

// Quartic Solver
#include "QuarticSolver_old.h"
//#include "RichRecPhotonTools/QuarticSolver.h"

//#define ONLY_CPU_VERSION
#define FPGA_VERSION

int tothepower = 24;
int multi = 160;//160;
int oternum = multi;
int nthr = 40;

//#define DEBUG
void CPU_version();

float cbrt(float , int );
timespec diff(timespec start, timespec end);

// Make an instance of the quartic solver
Rich::Rec::QuarticSolver qSolver;

Gaudi::XYZPoint sphReflPoint;


class Data
{
public:
  typedef std::vector<Data> Vector;
public:
  Gaudi::XYZPoint emissPnt;
  Gaudi::XYZPoint centOfCurv;
  Gaudi::XYZPoint virtDetPoint;
  double           radius;
public:
  Data() 
  {
    // randomn generator
    static std::default_random_engine gen;
    // Distributions for each member
    static std::uniform_real_distribution<double> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
    static std::uniform_real_distribution<double> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
    static std::uniform_real_distribution<double> r_vdp_x(-3000,3000), r_vdp_y(-200,200),   r_vdp_z(8100,8200);
    static std::uniform_real_distribution<float>  r_rad(8500,8600);
    // setup data
    emissPnt     = Gaudi::XYZPoint( r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) );
    centOfCurv   = Gaudi::XYZPoint( r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)   );
    virtDetPoint = Gaudi::XYZPoint( r_vdp_x(gen),   r_vdp_y(gen),   r_vdp_z(gen)   );
    radius       = r_rad(gen);
  }
};

template< class TYPE >
void solve( const Data& data )
{

#if defined( DEBUG ) 
		 union {
			 float f;
			 //char c[16]; // make this large enough for any floating point value
			 char c[sizeof(float)]; // Edit: changed to this
		 } u;

 printf("data.emissPnt:\t\t %f \t %f \t %f \n",data.emissPnt.X(),data.emissPnt.Y(),data.emissPnt.Z());
		 std::printf("\t\t\t");
		 u.f = data.emissPnt.X();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.emissPnt.Y();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.emissPnt.Z();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\n\n");

 printf("data.centOfCurv:\t %f \t %f \t %f \n",data.centOfCurv.X(),data.centOfCurv.Y(),data.centOfCurv.Z());
		 std::printf("\t\t\t");
		 u.f = data.centOfCurv.X();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.centOfCurv.Y();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.centOfCurv.Z();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\n\n");
		
 printf("data.virtDetPoint:\t %f \t %f \t %f \n",data.virtDetPoint.X(),data.virtDetPoint.Y(),data.virtDetPoint.Z());
		 std::printf("\t\t\t");
		 u.f = data.virtDetPoint.X();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.virtDetPoint.Y();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = data.virtDetPoint.Z();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\n\n");
		
 printf("data.radius:\t\t %f \n",data.radius);
		 std::printf("\t\t\t");
		 u.f = data.radius;
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\n\n");
#endif
		 
  qSolver.solve<TYPE>( data.emissPnt, 
                       data.centOfCurv, 
                       data.virtDetPoint,
                       data.radius, 
                       sphReflPoint );
	
#if defined( DEBUG ) 	
 printf("sphReflPoint:\t\t %f \t %f \t %f \n\n\n",sphReflPoint.X(),sphReflPoint.Y(),sphReflPoint.Z());
		 std::printf("\t\t\t");
		 u.f = sphReflPoint.X();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = sphReflPoint.Y();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		 std::printf("\t");
		 u.f = sphReflPoint.Z();
		 for (int i = 0 ; i < sizeof(float) ; ++i ) std::printf( "%02X" , u.c[sizeof(float)-i-1] & 0x00FF );
		std::printf("\n\n");
#endif
		
}

template< class TYPE >
void solve( const Data::Vector & dataV )
{
#ifdef COMMENTS
  std::cout << "Solving Quartic Equation for " 
            << typeid(TYPE).name() 
            << " Photons ..." << std::endl;
#endif

  // iterate over the data and solve it...
  //for ( const auto& data : dataV ) { solve<TYPE>(data); }
  
  auto end = dataV.end();
  //#pragma omp parallel for ordered schedule(dynamic) num_threads(nthr)
  #pragma omp parallel for schedule(dynamic, 5000) num_threads(nthr)
  for (auto it = dataV.begin(); it < end; ++it) {
    solve<TYPE>(*it);
  }
  
}



struct __attribute__((packed)) __attribute__((aligned(16))) rich_photon_in
{
float emissionPoint_X;
float emissionPoint_Y;
float emissionPoint_Z;
float CoC_X;
float CoC_Y;
float CoC_Z;
float virtDetPoint_X;
float virtDetPoint_Y;
float virtDetPoint_Z;
float radius;
};


struct __attribute__((packed)) __attribute__((aligned(16))) rich_photon_out
{
float sphReflPoint_X;
float sphReflPoint_Y;
float sphReflPoint_Z;
}; 


struct rich_photon_in_cpu
{
  Gaudi::XYZPoint emissPnt;
  Gaudi::XYZPoint centOfCurv;
  Gaudi::XYZPoint virtDetPoint;
  float           radius;
};


struct rich_photon_out_cpu
{
  Gaudi::XYZPoint sphReflPoint;
}; 



using namespace aocl_utils;

// OpenCL runtime configuration
cl_platform_id platform = NULL;
unsigned num_devices = 0;
scoped_array<cl_device_id> device; // num_devices elements
cl_context context = NULL;
scoped_array<cl_command_queue> queue; // num_devices elements
cl_program program = NULL;
scoped_array<cl_kernel> kernel; // num_devices elements
#if USE_SVM_API == 0
scoped_array<cl_mem> dataIn_buf; // num_devices elements
scoped_array<cl_mem> dataOut_buf; // num_devices elements
#endif /* USE_SVM_API == 0 */

// Problem data.
unsigned N = pow(2,tothepower); // problem size
unsigned int nPhotons = N;
	
#if USE_SVM_API == 0
scoped_array<scoped_aligned_ptr<rich_photon_in> > dataIn; // num_devices elements
scoped_array<scoped_aligned_ptr<rich_photon_out> > dataOut; // num_devices elements
#else
scoped_array<scoped_aligned_ptr<rich_photon_in> > dataIn; // num_devices elements
scoped_array<scoped_aligned_ptr<rich_photon_out> > dataOut; // num_devices elements
#endif /* USE_SVM_API == 0 */
scoped_array<scoped_aligned_ptr<rich_photon_out> > dataOut_ref; // num_devices elements

scoped_array<unsigned> n_per_device; // num_devices elements

// Function prototypes
float rand_float();
bool init_opencl();
void init_problem();
void run();
void cleanup();

// Entry point.
int main(int argc, char **argv) {
  Options options(argc, argv);

#ifdef FPGA_VERSION 

  
  // Optional argument to specify the problem size.
  if(options.has("n")) {
    N = options.get<unsigned>("n");
  }

  
  // Initialize OpenCL.
  if(!init_opencl()) {
    return -1;
  }

  // Initialize the problem data.
  // Requires the number of devices to be known.
  init_problem();

  // Run the kernel.
  run();

  // Free the resources allocated
  cleanup();


#endif

#ifdef ONLY_CPU_VERSION
CPU_version();

printf("Done\n");
#endif  
  
  return 0;
}

/////// HELPER FUNCTIONS ///////

// Randomly generate a floating-point number between -10 and 10.
float rand_float() {
  return float(rand()) / float(RAND_MAX) * 20.0f - 10.0f;
}

// Initializes the OpenCL objects.
bool init_opencl() {
  cl_int status;

  printf("Initializing OpenCL\n");

  if(!setCwdToExeDir()) {
    return false;
  }

  // Get the OpenCL platform.
  platform = findPlatform("Intel(R) FPGA");
  if(platform == NULL) {
    printf("ERROR: Unable to find Intel(R) FPGA OpenCL platform.\n");
    return false;
  }

  // Query the available OpenCL device.
  device.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));
  printf("Platform: %s\n", getPlatformName(platform).c_str());
  printf("Using %d device(s)\n", num_devices);
  for(unsigned i = 0; i < num_devices; ++i) {
    printf("  %s\n", getDeviceName(device[i]).c_str());
  }

  // Create the context.
  context = clCreateContext(NULL, num_devices, device, &oclContextCallback, NULL, &status);
  checkError(status, "Failed to create context");

  // Create the program for all device. Use the first device as the
  // representative device (assuming all device are of the same type).
  std::string binary_file = getBoardBinaryFile("rich", device[0]);
  printf("Using AOCX: %s\n", binary_file.c_str());
  program = createProgramFromBinary(context, binary_file.c_str(), device, num_devices);

  // Build the program that was just created.
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  checkError(status, "Failed to build program");

  // Create per-device objects.
  queue.reset(num_devices);
  kernel.reset(num_devices);
  n_per_device.reset(num_devices);
#if USE_SVM_API == 0
  dataIn_buf.reset(num_devices);
  dataOut_buf.reset(num_devices);
#endif /* USE_SVM_API == 0 */



  for(unsigned i = 0; i < num_devices; ++i) {
    // Command queue.
    queue[i] = clCreateCommandQueue(context, device[i], CL_QUEUE_PROFILING_ENABLE, &status);
    checkError(status, "Failed to create command queue");

    // Kernel.
    const char *kernel_name = "rich";
    kernel[i] = clCreateKernel(program, kernel_name, &status);
    checkError(status, "Failed to create kernel");

    // Determine the number of elements processed by this device.
    n_per_device[i] = N / num_devices; // number of elements handled by this device

    // Spread out the remainder of the elements over the first
    // N % num_devices.
    if(i < (N % num_devices)) {
      n_per_device[i]++;
    }

#if USE_SVM_API == 0
    // Input buffers.
    dataIn_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY,n_per_device[i] * sizeof(rich_photon_in), NULL, &status);
    checkError(status, "Failed to create buffer for dataIn_buf");
	
    // Output buffer.
    dataOut_buf[i] = clCreateBuffer(context, CL_MEM_WRITE_ONLY,n_per_device[i] * sizeof(rich_photon_out), NULL, &status);
    checkError(status, "Failed to create buffer for dataOut_buf");
#else
    cl_device_svm_capabilities caps = 0;

    status = clGetDeviceInfo(
      device[i],
      CL_DEVICE_SVM_CAPABILITIES,
      sizeof(cl_device_svm_capabilities),
      &caps,
      0
    );
    checkError(status, "Failed to get device info");

    if (!(caps & CL_DEVICE_SVM_COARSE_GRAIN_BUFFER)) {
      printf("The host was compiled with USE_SVM_API, however the device currently being targeted does not support SVM.\n");
      // Free the resources allocated
      cleanup();
      return false;
    }
#endif /* USE_SVM_API == 0 */
  }

  return true;
}

// Initialize the data for the problem. Requires num_devices to be known.
void init_problem() {
  if(num_devices == 0) {
    checkError(-1, "No devices");
  }

  dataIn.reset(num_devices);
  dataOut.reset(num_devices);

  dataOut_ref.reset(num_devices);


  // Generate input vectors A and B and the reference output consisting
  // of a total of N elements.
  // We create separate arrays for each device so that each device has an
  // aligned buffer.
  for(unsigned i = 0; i < num_devices; ++i) {
#if USE_SVM_API == 0
    dataIn[i].reset(n_per_device[i]);
	
    dataOut[i].reset(n_per_device[i]);
	
    dataOut_ref[i].reset(n_per_device[i]);


    for(unsigned j = 0; j < n_per_device[i]; ++j) {
      dataIn[i][j].emissionPoint_X = -449.665302;
      dataIn[i][j].emissionPoint_Y = -49.619842;
      dataIn[i][j].emissionPoint_Z = 10065.768894;
      dataIn[i][j].CoC_X = 116.498232;
      dataIn[i][j].CoC_Y = 17.387716;
      dataIn[i][j].CoC_Z = 3367.886472;
      dataIn[i][j].virtDetPoint_X = -2953.810884;
      dataIn[i][j].virtDetPoint_Y = 11.880077;
      dataIn[i][j].virtDetPoint_Z = 8103.457211;
      dataIn[i][j].radius = 8538.341797;
	  
      dataOut_ref[i][j].sphReflPoint_X = -1966.998350;
      dataOut_ref[i][j].sphReflPoint_Y = -43.751807;
      dataOut_ref[i][j].sphReflPoint_Z = 11647.897214;
    }


    printf("Create Ref calc photon data\n\n");	
	Data::Vector dataV;
	dataV.reserve( nPhotons );
	// Construct the data to work on
	std::cout << "Creating " << nPhotons << " random photons ..." << std::endl;
	for ( unsigned int i = 0; i < nPhotons; ++i ) { dataV.push_back( Data() ); }

	
	printf("Multi loop factor: %i\n",multi);

	printf("Used CPU threads: %i\n\n",nthr);
	
//OLD version
/*
	// REF Time meas CPU
    const double start_time_ref = getCurrentTimestamp();
	
	#pragma omp parallel for ordered schedule(dynamic) num_threads(nthr)
	for(int loopref=0;loopref<multi;loopref++)
	{
	solve<float>( dataV );
	}
	const double end_time_ref = getCurrentTimestamp();
	printf("\nTime REF: %0.3f ms\n", (end_time_ref - start_time_ref) * 1e3);
*/
// NEW Version
CPU_version();
// end NEW Version	

	
	
    for(unsigned j = 0; j < n_per_device[i]; ++j) {
      dataOut_ref[i][j].sphReflPoint_X = -1966.998350;
      dataOut_ref[i][j].sphReflPoint_Y = -43.751807;
      dataOut_ref[i][j].sphReflPoint_Z = 11647.897214;	  
    }
	
	
#else
    dataIn[i].emissionPoint_X.reset(context, n_per_device[i]);
    dataIn[i].emissionPoint_Y.reset(context, n_per_device[i]);
    dataIn[i].emissionPoint_Z.reset(context, n_per_device[i]);
    dataIn[i].CoC_X.reset(context, n_per_device[i]);
    dataIn[i].CoC_Y.reset(context, n_per_device[i]);
    dataIn[i].CoC_Z.reset(context, n_per_device[i]);
    dataIn[i].virtDetPoint_X.reset(context, n_per_device[i]);
    dataIn[i].virtDetPoint_Y.reset(context, n_per_device[i]);
    dataIn[i].virtDetPoint_Z.reset(context, n_per_device[i]);
    dataIn[i].radius.reset(context, n_per_device[i]);
	
    dataOut[i].sphReflPoint_X.reset(context, n_per_device[i]);
    dataOut[i].sphReflPoint_Y.reset(context, n_per_device[i]);
    dataOut[i].sphReflPoint_Z.reset(context, n_per_device[i]);
	
    dataOut_ref[i].sphReflPoint_X.reset(context, n_per_device[i]);
    dataOut_ref[i].sphReflPoint_Y.reset(context, n_per_device[i]);
    dataOut_ref[i].sphReflPoint_Z.reset(context, n_per_device[i]);

    cl_int status;

    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE,(void *)dataIn[i], n_per_device[i] * sizeof(rich_photon_in), 0, NULL, NULL);
    checkError(status, "Failed to map dataIn");


    for(unsigned j = 0; j < n_per_device[i]; ++j) {
      dataIn[i][j].emissionPoint_X = -449.665302;
      dataIn[i][j].emissionPoint_Y = -49.619842;
      dataIn[i][j].emissionPoint_Z = 10065.768894;
      dataIn[i][j].CoC_X = 116.498232;
      dataIn[i][j].CoC_Y = 17.387716;
      dataIn[i][j].CoC_Z = 3367.886472;
      dataIn[i][j].virtDetPoint_X = -2953.810884;
      dataIn[i][j].virtDetPoint_Y = 11.880077;
      dataIn[i][j].virtDetPoint_Z = 8103.457211;
      dataIn[i][j].radius = 8538.341797;
	  
      dataOut_ref[i][j].sphReflPoint_X = -1966.998350;
      dataOut_ref[i][j].sphReflPoint_Y = -43.751807;
      dataOut_ref[i][j].sphReflPoint_Z = 11647.897214;
    }

    status = clEnqueueSVMUnmap(queue[i], (void *)dataIn[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap dataIn");

#endif /* USE_SVM_API == 0 */
  }
}

void run() {
  cl_int status;

  const double start_time = getCurrentTimestamp();

  for(int loopcount=0;loopcount<multi;loopcount++)
  {
  // Launch the problem for each device.
  scoped_array<cl_event> kernel_event(num_devices);
  scoped_array<cl_event> finish_event(num_devices);

  for(unsigned i = 0; i < num_devices; ++i) {

#if USE_SVM_API == 0
    // Transfer inputs to each device. Each of the host buffers supplied to
    // clEnqueueWriteBuffer here is already aligned to ensure that DMA is used
    // for the host-to-device transfer.
    cl_event write_event[1];
    status = clEnqueueWriteBuffer(queue[i], dataIn_buf[i], CL_FALSE,0, n_per_device[i] * sizeof(rich_photon_in), dataIn[i], 0, NULL, &write_event[0]);
    checkError(status, "Failed to transfer dataIn");

#endif /* USE_SVM_API == 0 */

    // Set kernel arguments.
    unsigned argi = 0;

#if USE_SVM_API == 0
    status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &dataIn_buf[i]);
    checkError(status, "Failed to set argument %d", argi - 1);


    status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &dataOut_buf[i]);
    checkError(status, "Failed to set argument %d", argi - 1);

#else
    status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)dataIn[i]);
    checkError(status, "Failed to set argument %d", argi - 1);

	
    status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)dataOut[i]);
    checkError(status, "Failed to set argument %d", argi - 1);
#endif /* USE_SVM_API == 0 */

    // Enqueue kernel.
    // Use a global work size corresponding to the number of elements to add
    // for this device.
    //
    // We don't specify a local work size and let the runtime choose
    // (it'll choose to use one work-group with the same size as the global
    // work-size).
    //
    // Events are used to ensure that the kernel is not launched until
    // the writes to the input buffers have completed.	
    const size_t global_work_size = n_per_device[i];
#ifdef COMMENTS
    printf("Launching for device %d (%zd elements)\n", i, global_work_size);
#endif

	
#if USE_SVM_API == 0
    status = clEnqueueNDRangeKernel(queue[i], kernel[i], 1, NULL,&global_work_size, NULL, 1, write_event, &kernel_event[i]);
#else
    status = clEnqueueNDRangeKernel(queue[i], kernel[i], 1, NULL,&global_work_size, NULL, 0, NULL, &kernel_event[i]);
#endif /* USE_SVM_API == 0 */
    checkError(status, "Failed to launch kernel");

#if USE_SVM_API == 0
    // Read the result. This the final operation.
    status = clEnqueueReadBuffer(queue[i], dataOut_buf[i], CL_FALSE,0, n_per_device[i] * sizeof(rich_photon_out), dataOut[i], 1, &kernel_event[i], &finish_event[i]);
	
    // Release local events.
    clReleaseEvent(write_event[0]);

#else
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_READ,(void *)dataOut[i], n_per_device[i] * sizeof(rich_photon_out), 0, NULL, NULL);
    checkError(status, "Failed to map dataOut");
	clFinish(queue[i]);
#endif /* USE_SVM_API == 0 */
  }

  // Wait for all devices to finish.
  clWaitForEvents(num_devices, finish_event);

#ifdef COMMENTS	
  // Get kernel times using the OpenCL event profiling API.
  for(unsigned i = 0; i < num_devices; ++i) {
    cl_ulong time_ns = getStartEndTime(kernel_event[i]);
    printf("Kernel time (device %d): %0.3f ms\n", i, double(time_ns) * 1e-6);
  }
#endif

  // Release all events.
  for(unsigned i = 0; i < num_devices; ++i) {
    clReleaseEvent(kernel_event[i]);
    clReleaseEvent(finish_event[i]);
  }
  
  } //endloop
  
  const double end_time = getCurrentTimestamp();

  // Wall-clock time taken.
  printf("\nTime: %0.3f ms\n", (end_time - start_time) * 1e3);


  // Verify results.
  bool pass = true;
  for(unsigned i = 0; i < num_devices && pass; ++i) {
    for(unsigned j = 0; j < n_per_device[i] && pass; ++j) {
      if((fabsf(dataOut[i][j].sphReflPoint_X - dataOut_ref[i][j].sphReflPoint_X) > 1.0e-2f) || (fabsf(dataOut[i][j].sphReflPoint_Y - dataOut_ref[i][j].sphReflPoint_Y) > 1.0e-2f) || (fabsf(dataOut[i][j].sphReflPoint_Z - dataOut_ref[i][j].sphReflPoint_Z) > 1.0e-2f) ) {
        printf("Failed verification @ device %d, index %d\nOutput: %f\nReference: %f\n",
            i, j, dataOut[i][j].sphReflPoint_X, dataOut_ref[i][j].sphReflPoint_X);
        printf("Failed verification @ device %d, index %d\nOutput: %f\nReference: %f\n",
            i, j, dataOut[i][j].sphReflPoint_Y, dataOut_ref[i][j].sphReflPoint_Y);
        printf("Failed verification @ device %d, index %d\nOutput: %f\nReference: %f\n",
            i, j, dataOut[i][j].sphReflPoint_Z, dataOut_ref[i][j].sphReflPoint_Z);
        pass = false;
      }
    }
  }

#if USE_SVM_API == 1
  for (unsigned i = 0; i < num_devices; ++i) {
    status = clEnqueueSVMUnmap(queue[i], (void *)dataOut[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap dataOut");
  }
#endif /* USE_SVM_API == 1 */
  printf("\nVerification: %s\n", pass ? "PASS" : "FAIL");
}

// Free the resources allocated during initialization
void cleanup() {
  for(unsigned i = 0; i < num_devices; ++i) {
    if(kernel && kernel[i]) {
      clReleaseKernel(kernel[i]);
    }
    if(queue && queue[i]) {
      clReleaseCommandQueue(queue[i]);
    }
#if USE_SVM_API == 0
    if(dataIn_buf && dataIn_buf[i]) {
      clReleaseMemObject(dataIn_buf[i]);
    }
    if(dataOut_buf && dataOut_buf[i]) {
      clReleaseMemObject(dataOut_buf[i]);
    }
#else
    if(dataIn[i].get())
      dataIn[i].reset();
    if(dataOut[i].get())
      dataOut[i].reset();
#endif /* USE_SVM_API == 0 */
  }

  if(program) {
    clReleaseProgram(program);
  }
  if(context) {
    clReleaseContext(context);
  }
}


void CPU_version(){
	printf("ONLY CPU VERSION\n\n");
	
	printf("Create Ref calc photon data\n\n");	

	// REF Time meas CPU
    const double start_time_ref_buff = getCurrentTimestamp();
	
	struct rich_photon_in_cpu * richIn = new rich_photon_in_cpu[nPhotons];
//	richIn = (struct rich_photon_in_cpu *) malloc (nPhotons+1);
	

	struct rich_photon_out_cpu * richOut = new rich_photon_out_cpu[nPhotons];
//	richOut = (struct rich_photon_out_cpu*) malloc (16*4*nPhotons+1);	

	const double end_time_ref_buff = getCurrentTimestamp();
	printf("\nTime REF buff create: %0.3f ms\n", (end_time_ref_buff - start_time_ref_buff) * 1e3);	

	static std::default_random_engine gen;
    // Distributions for each member
    static std::uniform_real_distribution<float> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
    static std::uniform_real_distribution<float> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
    static std::uniform_real_distribution<float> r_vdp_x(-3000,3000), r_vdp_y(-200,200),   r_vdp_z(8100,8200);
    static std::uniform_real_distribution<float>  r_rad(8500,8600);	

	// REF Time meas CPU
    const double start_time_ref_fill = getCurrentTimestamp();
	std::cout << "Creating " << nPhotons << " random photons ..." << std::endl;	
	//#pragma omp parallel for schedule(static) num_threads(nthr)
	#pragma omp simd parallel for schedule(static) num_threads(nthr)
	for(int i = 0;i<nPhotons; i++)
	{

		richIn[i].emissPnt     = Gaudi::XYZPoint( r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) );
		richIn[i].centOfCurv   = Gaudi::XYZPoint( r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)   );
		richIn[i].virtDetPoint = Gaudi::XYZPoint( r_vdp_x(gen),   r_vdp_y(gen),   r_vdp_z(gen)   );
		richIn[i].radius       = r_rad(gen);

	}

	const double end_time_ref_fill = getCurrentTimestamp();
	printf("\nTime REF fill: %0.3f ms\n", (end_time_ref_fill - start_time_ref_fill) * 1e3);
	
	
	printf("Multi loop factor: %i\n",multi);

	printf("Used CPU threads: %i\n\n",nthr);

	printf("INLINE pointer version:\n");
	
	// REF Time meas CPU
    const double start_time_ref = getCurrentTimestamp();

	#pragma omp parallel for schedule(static) num_threads(nthr)	
	for(int oter=0;oter<oternum;oter++){
//	#pragma omp parallel for schedule(static) num_threads(nthr)
	#pragma omp simd
	for(int i = 0;i<nPhotons; i++)
	{
				   
		Gaudi::XYZPoint emissionPoint 	= richIn[i].emissPnt;
        Gaudi::XYZPoint CoC				= richIn[i].centOfCurv;
        Gaudi::XYZPoint virtDetPoint	= richIn[i].virtDetPoint;
        float radius					= richIn[i].radius;
        Gaudi::XYZPoint sphReflPoint;
							   
							   
        using namespace Rich::Maths::FastRoots;

        typedef Eigen::Matrix< float , 3 , 1 > Eigen3Vector;
        typedef Eigen::Matrix< float , 4 , 1 > Eigen4Vector;

        bool OK = true;

        // vector from mirror centre of curvature to assumed emission point
        const Eigen4Vector evec( emissionPoint.x() - CoC.x(),
                                 emissionPoint.y() - CoC.y(),
                                 emissionPoint.z() - CoC.z(), 0 );
        const auto e2 = evec.dot(evec);

        // vector from mirror centre of curvature to virtual detection point
        const Eigen4Vector dvec( virtDetPoint.x() - CoC.x(),
                                 virtDetPoint.y() - CoC.y(),
                                 virtDetPoint.z() - CoC.z(), 0 );
        const auto d2 = dvec.dot(dvec);

       // if ( ( (e2 < 1e-99) || (d2 < 1e-99) ) ) 
       // {
       //   OK = false;
       // }
       // else
        {

          // various quantities needed to create quartic equation
          // see LHCB/98-040 section 3, equation 3
          const auto e        = std::sqrt(e2);
          const auto d        = std::sqrt(d2);
          const auto cosgamma = evec.dot(dvec) / (e*d);
          const auto singamma = std::sqrt( 1.0 - cosgamma*cosgamma );
          const auto dx       = d * cosgamma;
          const auto edx      = e + dx;
          const auto dy       = d * singamma;
          const auto dy2      = dy * dy;
          const auto r2       = radius * radius;
          
          // Fill array for quartic equation
          const auto a0     =     4.0f * e2 * d2; 
          const auto inv_a0 =   ( 1.0f / a0 );
          const auto a1     = - ( 4.0f * e2 * dy * radius ) * inv_a0;
          const auto a2     =   ( (dy2 * r2) + ( edx * edx * r2 ) - a0 ) * inv_a0;
          const auto a3     =   ( 2.0f * e * dy * (e-dx) * radius ) * inv_a0;
          const auto a4     =   ( ( e2 - r2 ) * dy2 ) * inv_a0;

/*          
          // use simplified RICH version of quartic solver
          const auto sinbeta = solve_quartic_RICH<TYPE>( a1, // a
                                                         a2, // b
                                                         a3, // c
                                                         a4  // d
                                                         );
*/        
		  
			const float a = a1;
            const float b = a2;
            const float c = a3;
            const float d_f = a4;
			
        using namespace Rich::Maths::FastRoots;

        const auto r4 = 1.0f / 4.0f;
        const auto q2 = 1.0f / 2.0f;
        const auto q8 = 1.0f / 8.0f;
        const auto q1 = 3.0f / 8.0f;
        const auto q3 = 3.0f / 16.0f;

        const auto UU = -( std::sqrt(3.0) / 2.0 );

        const auto aa = a * a;
        const auto pp = b - q1 * aa;
        const auto qq = c - q2 * a * (b - r4 * aa);
        const auto rr = d_f - r4 * (a * c - r4 * aa * (b - q3 * aa));
        const auto rc = q2 * pp;
        const auto sc = r4 * (r4 * pp * pp - rr);
        const auto tc = -(q8 * qq * q8 * qq);

        const auto qcub = (rc * rc - 3 * sc);
        const auto rcub = (2.0f * rc * rc * rc - 9 * rc * sc + 27.0f * tc);

        const auto Q = qcub / 9.0f;
        const auto R = rcub / 54.0f;

        const auto Q3 = Q * Q * Q;
        const auto R2 = R * R;

        const auto sgnR = ( R >= 0 ? -1 : 1 );

        const auto A = sgnR * fast_cbrt( (float)( fabs(R) + std::sqrt( fabs(R2-Q3) ) ) );

        const auto B = Q / A;

        const auto u1 = -0.5f * (A + B) - rc / 3.0f;
        const auto u2 = UU * fabs(A-B);

        std::complex<float> w1(u1,u2), w2(u1,-u2), w3(0,0);

        w1 = std::sqrt(w1);
        w2 = std::sqrt(w2);

		
		//if(w1 != 0 && w2 != 0)
        if ( std::abs( w1 * w2 ) != 0.0 )
		{
          w3 = ( -qq / 8.0f ) / ( w1 * w2 ) ;
        }

        const float res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);


		const auto sinbeta = ( res >  1.0 ?  1.0 : res < -1.0 ? -1.0 : res );
				 
		  
          // (normalised) normal vector to reflection plane
          auto n = evec.cross3(dvec);
          
          //n /= std::sqrt( n.dot(n) );
          n *= vdt::fast_isqrtf( n.dot(n) );
          
          // construct rotation transformation
          // Set vector magnitude to radius
          // rotate vector and update reflection point
          const Eigen::AngleAxis<float> angleaxis( vdt::fast_asinf(sinbeta),Eigen3Vector(n[0],n[1],n[2]) );
		  
          sphReflPoint = CoC + Gaudi::XYZVector( angleaxis * Eigen3Vector(evec[0],evec[1],evec[2]) * ( radius / e ) );
          
		  richOut[i].sphReflPoint = sphReflPoint;
		  

		  //printf("%i : (%5.2f,\t %5.2f,\t %5.2f) \n",i,richOut[i].sphReflPoint.X(),richOut[i].sphReflPoint.Y(),richOut[i].sphReflPoint.Z());
		  
        }


    }

	}
	
	  
	const double end_time_ref = getCurrentTimestamp();
	printf("\nTime REF: %0.3f ms\n", (end_time_ref - start_time_ref) * 1e3);
	
	//printf("DLD\n\n");
	
	
	delete[] richIn;
	delete[] richOut;							   
	
	//printf("DLD\n\n");	
	
	
//		printf("sphReflPoint:\t\t %f \t %f \t %f \n\n\n",dataV[0].data.sphReflPoint.X(),dataV[0].data.sphReflPoint.Y(),dataV[0].data.sphReflPoint.Z());
//		printf("\n\n");

/*	
	
	printf("Multi loop factor: %i\n",multi);

	printf("Used CPU threads: %i\n\n",nthr);
	
	// REF Time meas CPU
    const double start_time_ref = getCurrentTimestamp();


	solve<float>( dataV );

	
	const double end_time_ref = getCurrentTimestamp();
	printf("\nTime REF: %0.3f ms\n", (end_time_ref - start_time_ref) * 1e3);
*/
	
}

